﻿namespace test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.rbtnDiagnose = new System.Windows.Forms.RadioButton();
            this.rbtnRegister = new System.Windows.Forms.RadioButton();
            this.nudNumMax = new System.Windows.Forms.NumericUpDown();
            this.lblMaxNum = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.gbMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumMax)).BeginInit();
            this.SuspendLayout();
            // 
            // gbMode
            // 
            this.gbMode.Controls.Add(this.rbtnDiagnose);
            this.gbMode.Controls.Add(this.rbtnRegister);
            this.gbMode.Location = new System.Drawing.Point(90, 12);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(104, 76);
            this.gbMode.TabIndex = 0;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "Mode";
            // 
            // rbtnDiagnose
            // 
            this.rbtnDiagnose.AutoSize = true;
            this.rbtnDiagnose.Location = new System.Drawing.Point(6, 42);
            this.rbtnDiagnose.Name = "rbtnDiagnose";
            this.rbtnDiagnose.Size = new System.Drawing.Size(70, 17);
            this.rbtnDiagnose.TabIndex = 1;
            this.rbtnDiagnose.TabStop = true;
            this.rbtnDiagnose.Text = "Diagnose";
            this.rbtnDiagnose.UseVisualStyleBackColor = true;
            // 
            // rbtnRegister
            // 
            this.rbtnRegister.AutoSize = true;
            this.rbtnRegister.Location = new System.Drawing.Point(6, 19);
            this.rbtnRegister.Name = "rbtnRegister";
            this.rbtnRegister.Size = new System.Drawing.Size(64, 17);
            this.rbtnRegister.TabIndex = 0;
            this.rbtnRegister.TabStop = true;
            this.rbtnRegister.Text = "Register";
            this.rbtnRegister.UseVisualStyleBackColor = true;
            // 
            // nudNumMax
            // 
            this.nudNumMax.Location = new System.Drawing.Point(105, 108);
            this.nudNumMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudNumMax.Name = "nudNumMax";
            this.nudNumMax.Size = new System.Drawing.Size(74, 20);
            this.nudNumMax.TabIndex = 1;
            this.nudNumMax.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // lblMaxNum
            // 
            this.lblMaxNum.AutoSize = true;
            this.lblMaxNum.Location = new System.Drawing.Point(97, 95);
            this.lblMaxNum.Name = "lblMaxNum";
            this.lblMaxNum.Size = new System.Drawing.Size(91, 13);
            this.lblMaxNum.TabIndex = 2;
            this.lblMaxNum.Text = "Number Maximum";
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(105, 174);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 3;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 230);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(278, 249);
            this.textBox1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 480);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.lblMaxNum);
            this.Controls.Add(this.nudNumMax);
            this.Controls.Add(this.gbMode);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbMode.ResumeLayout(false);
            this.gbMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumMax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMode;
        private System.Windows.Forms.RadioButton rbtnDiagnose;
        private System.Windows.Forms.RadioButton rbtnRegister;
        private System.Windows.Forms.NumericUpDown nudNumMax;
        private System.Windows.Forms.Label lblMaxNum;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox textBox1;
    }
}

