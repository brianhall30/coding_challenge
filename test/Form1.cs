﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form1 : Form
    {
        IEvent runMode;
        IEvaluate eval;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            textBox1.Clear();

            var checkedButton = gbMode.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.Checked);

            if (checkedButton.Text == "Register")
            {
                runMode = new Register();
                eval = new RegisterEval();
            }
            else
            {
                runMode = new Diagnose();
                eval = new DiagnoseEval();
            }

            var output = runMode.Run((int)nudNumMax.Value, eval);

            SetTextBox(output);
        }

        private void SetTextBox(List<string> output)
        {
            foreach (var item in output)
            {
                textBox1.AppendText(item + Environment.NewLine);
            }
        }
    }
}
