﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Diagnose : IEvent
    {
        List<string> output = new List<string>();

        public List<string> Run(int number_max, IEvaluate eval)
        {
            for (int i = 1; i <= number_max; i++)
            {
               output.Add(eval.Evaluate(i));
            }
            return output;
        }
    }
}
