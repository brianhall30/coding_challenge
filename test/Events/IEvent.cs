﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    interface IEvent
    {
        List<string> Run(int number_max, IEvaluate eval);
    }
}
