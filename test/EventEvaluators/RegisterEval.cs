﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class RegisterEval : IEvaluate
    {
        public string Evaluate(int num)
        {
            string output;

            if (((num % 3) == 0) && ((num % 5) == 0))
            {
                output = "Register Patient";
            }
            else if (((num % 3) == 0))
            {
                output = "Register";
            }
            else if (((num % 5) == 0))
            {
                output = "Patient";
            }
            else
            {
                output = num.ToString();
            }
            return output;
        }
    }
}
