﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class DiagnoseEval : IEvaluate
    {
        public string Evaluate(int num)
        {
            string output;

            if (((num % 2) == 0) && ((num % 7) == 0))
            {
                output = "Diagnose Patient";
            }
            else if (((num % 2) == 0))
            {
                output = "Diagnose";
            }
            else if (((num % 7) == 0))
            {
                output = "Patient";
            }
            else
            {
                output = num.ToString();
            }
            return output;
        }
    }
}
